
//default builds the super aptitude admin when app startup
import '../imports/startup/server'
import { Meteor } from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import {AptitudeAdminDb} from '../imports/api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {AccountDb} from '../imports/api/account/collection/collection.js'
import {UserAccountDb} from '../imports/api/account/collection/userAccount.js'
import {WorkflowDataDb} from '../imports/api/formdata/collection/workflowData.collection.js'
import {ClientUserDb} from '../imports/api/clientUser/collection/clientUser.collection.js';
import moment from 'moment';
Meteor.startup(() => {



   Meteor.setInterval(function(){
  
var date = new moment().format('MM/DD/YYYY HH:mm A');

    
  var datas=WorkflowDataDb.find({'action.status':'assigned','action.ETA':{$gte:date}}).fetch();
  
   _.each(datas,function(data){
        var msgdate=new moment(data.action.lastmsg).format('MM/DD/YYYY HH:mm');
        var lastmsg=new moment(msgdate).add(data.action.msgInterval,'hours').format('MM/DD/YYYY HH:mm');
        var current=new moment().format('MM/DD/YYYY HH:mm')
        if(moment(lastmsg).isSame(current)){
          var receipt=ClientUserDb.findOne({user:data.action.user});
        var mail={ to: receipt.email,
        cc:[],
        bcc:[],
  	    subject:"ETA Notificaton",
  	    text: "Today is the last day  for the task to get completed"}
        Meteor.call('sendEmail',mail,function(err){
          if(err)
          console.log(err);
        });
        Meteor.call('updateLastmsg',{lastmsg:lastmsg,id:data.id},function(err){
          if(!err){
            console.log('update');
          }
        })
        }

        
   });
 
},10000);
  if(!Meteor.users.findOne()){
    
    const userId=Accounts.createUser({email:"admin@yahoo.com",password:"aptitude123"})
      Roles.addUsersToRoles(userId,['aptitude-admin']);
      let adminId=AptitudeAdminDb.insert({user:userId,contactName:"john",name:"john",mobile:'53453453',createdBy:null,companies:[]})
    }
});
