import {WorkflowDb} from "./collection/workflow.collection.js"

Meteor.methods({
  'addWorkFlow':function(record){
    return WorkflowDb.insert(record);
  },
  "saveFlowchart":function (data) {
    console.log(data)
   WorkflowDb.update({_id:data.workflow},{$set:{charts:data.charts,flowchart:data.flowchartData,action:data.action,emails:data.emails}})
  },
  'deleteWorkFlow':function(id){
    WorkflowDb.remove({_id:id});
  },
  'editWorkFlow':function(record){
  return  WorkflowDb.update({_id:record.id},{$set:record.data})
},
 updateWorkflowChart:function(data){
   return WorkflowDb.update({_id:data.id},{$set:{charts:data.charts}})
 },
 'updateWorkflowAxis':function(data){
   return WorkflowDb.update({_id:data.id},{$set:{axis:{xAxis:data.xAxis,yAxis:data.yAxis}}})
 }
})
