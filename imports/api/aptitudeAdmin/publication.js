import {AptitudeAdminDb} from './collection/aptitude.admin.collection.js'

Meteor.publish('getAptitudeAdmin',function(){
  if(this.userId){
    return AptitudeAdminDb.find()
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
 
  
  
})
