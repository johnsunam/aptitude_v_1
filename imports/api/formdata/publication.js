import {FormDataDb} from './collection/formdata.collection.js';
import {WorkflowDataDb} from './collection/workflowData.collection.js'
Meteor.publish('getFormData',function(){
  if(this.userId){
     return FormDataDb.find();
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
 
})

Meteor.publish('getWorkflowDatas',function(){
  if(this.userId){
     return WorkflowDataDb.find();
  }
  else{
    throw new Meteor.Error('list.unauthorized',
      'This list doesn\'t belong to you.');
  }
 
})