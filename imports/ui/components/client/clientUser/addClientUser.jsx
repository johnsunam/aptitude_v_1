
import React ,{Component} from 'react'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
import MyInput from '../../common/validator.js'
import {Checkbox, CheckboxGroup} from 'react-checkbox-group';
import ReactConfirmAlert, { confirmAlert } from 'react-confirm-alert'; // Import 
import 'react-confirm-alert/src/react-confirm-alert.css'
var Confirm = require('react-confirm-bootstrap');

export default class AddClientUser extends Component {
  constructor(props) {
   super(props)
   this.state={saveResult:false,
  edit:this.props.edit,
  clientUser:this.props.clientUser,
  isShowMessage: false,
  userCode:"",
  dob:'',
  address:'',
  mobile:'',
  email:'',
  secQuestion:'',
  secAnswer:'',
  userTypes:'',
  roles:[],
  companies:[],
  type:'',
  showMessage:'',
  message:''
  }

  }

   componentDidMount(){
     console.log(this.props);
      this.state.edit?this.setState({name:this.props.clientUser.name,
      dob:this.props.clientUser.dob,
      address:this.props.clientUser.address,
      contact:this.props.clientUser.contact,
      email:this.props.clientUser.email,
      secQuestion:this.props.clientUser.secQuestion,
      secAnswer:this.props.clientUser.secAnswer,
      roles:this.props.clientUser.roles,
      userTypes:this.props.clientUser.userTypes,
      companies:this.props.clientUser.companies,
      type:this.props.clientUser.type
    }):'';

   }

  editClientUser(){

  }
  // saving clientUser to clientUserDb
  confirmTrigger(e){
    var self=this;
    
	  let name=e.name,
		dob=e.dob,
		address=e.address,
		contact=e.contact,
		email=e.email,
		secQuestion=e.secquest,
		secAnswer=e.secAnswer,
		roles=this.state.roles,
    companies=this.state.companies,
		userTypes = this.state.userTypes,
    type=this.state.type
    let userCode=Random.hexString(7);
    let client=window.sessionStorage.getItem('user');
    let createdBy=window.localStorage.getItem('user');
    let user=client!= 'null'?client:Meteor.userId();
    let company=window.localStorage.getItem('company')
    let record=this.props.edit?{id:this.props.clientUser._id,data:{compaies:companies,sup:sup,name:name,dob:dob,status:status,address:address,contact:contact,email:email,secQuestion:secQuestion, secAnswer:secAnswer,roles:roles, userTypes:userTypes}}:
    {user:user,data:{code:userCode,createdBy:createdBy,name:name,dob:dob,status:status,address:address,contact:contact,email:email,secQuestion:secQuestion, secAnswer:secAnswer,userTypes:userTypes,roles:roles,companies:[company]}}
     
  
    if(roles.length!=0){
      $('#submitform').trigger('click');
      this.setState({record:record})
     
    }
    else {
      Alert.warning('Add atleast one role', {
             position: 'top-right',
             effect: 'bouncyflip',
             timeout: 1000
         })
    }


    
  }
  enableButton() {
    this.setState({ canSubmit: true });
  }
  disableButton() {
    this.setState({ canSubmit: false });
  }

  shouldComponentUpdate(nextProps, nextState){
    let self=this;
    Tracker.autorun(function(){
      if(Session.equals('confirm',true)){
        if(Session.get('res')==true){
          self.setState({showMessage:true,message:"User Registered Sucessfully"})
          $('.message').addClass('su')
        }else{
           self.setState({showMessage:true,message:"Email already exits"})
          $('.message').addClass('er')
        }
         $('select').prop('selectedIndex',0);
    $('#name').val('');
    $('#contactNo').val('');
    $('#secAnswer').val('');
    $('#secquest').val('');
    $('#dob').val('');
    $('#address').val('');

              Session.set('confirm',false)
      }
    })

    return true;
}
  submit(){
    let obj= new crudClass();
   this.state.edit ? obj.create('editClientUser',this.state.record) : obj.create('addClientUser',this.state.record);
   
  }

  render(){
    let roles=this.props.data.roles?this.props.data.roles:[]
    let submitButton=<button type="submit" className="btn btn-primary btn-sm" disabled={!this.state.canSubmit} ><span>Save</span></button>
   return(<div>
     <section className="content-header">
     <h1>{this.props.edit?"Edit User":"Add User"} </h1>
   <div className="col-md-offset-1 message">{this.state.message}</div>
   </section>
   <section className="content">
   <div className="box-body">
       
   <Formsy.Form ref="form" onValidSubmit={this.confirmTrigger.bind(this)} id="addClient" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
           <div className="form-group">
             <label for="name"> Name</label>
             <MyInput type="text" className="form-control" ref="name" help="Enter User Name" value={this.state.name} id="name" name="name" placeholder="Name"  required/>
           </div>
           <div className="form-group">
             <label for="dob">Date of Birth</label>
             <MyInput type="date" name="dob" className="form-control" help="Enter Birthday" value={this.state.dob} id="dob" placeholder="DOB" ref="dob" required/>
           </div>
           <div className="form-group">
             <label for="address">Address</label>
             <MyInput type="text" name="address" help="Enter Address" className="form-control" value={this.state.address} id="address" placeholder="Address" required ref="address"/>
           </div>
           <div className="form-group">
             <label for="mobile">Contact #</label>
             <MyInput required name="contactNo" type="number" help="Enter Contact" className="form-control" id="contactNo" value={this.state.contactNo} placeholder="Contact Number" ref="contactNo"/>
           </div>
           <div className="form-group">
             <label for="email">E-Mail</label>
             <MyInput required name="email" type="email" className="form-control" help="Enter Email" id="email" value={this.state.email} placeholder="E-Mail" ref="email"/>
           </div>
           <div className="form-group">
             <label for="secQuestion">Security Question</label>
             <MyInput required name="secquest" type="text" help="Enter Security Question" className="form-control" id="secquest" placeholder="Security Question" ref="secquest"/>
          
           </div>
           <div className="form-group">
             <label for="secAnswer">Security Answer</label>
             <MyInput required name="secAnswer" type="text" help="Enter Security answer" className="form-control" id="secAnswer" placeholder="Security Answer" ref="secAnswer"/>
           </div>

          <div className="form-group">
             <label for="userType">User Type</label>
             <CheckboxGroup nam="types" onChange={(userTypes)=>{
               this.setState({userTypes:userTypes})
             }}>
             <div><label><Checkbox value="App-User"/>App-User</label></div>
             <div><label><Checkbox value="client"/>Admin</label></div>
             </CheckboxGroup>
             </div>
           <div className="form-group">
             <label>Assign Roles</label>
             <CheckboxGroup name="roles" value={this.state.roles} onChange={(newroles)=>{
               this.setState({roles:newroles})
             }}>
             {roles.map((role)=>{
             return(<div><label><Checkbox value={role}/>{role}</label></div>)
             })}
             </CheckboxGroup>
             <h4>Added Roles</h4>
             <ul  style={{"listStyleType": "none"}}>
             {this.state.roles.map((role)=>{
               return(<li>{role} <a href="#" id={role} onClick={(e)=>{
                 let roles=this.state.roles
                 let addedRoles=_.without(roles,e.target.id)
                 this.setState({roles:addedRoles})
               }}><i className="fa fa-times" id={role}></i></a></li>)
             })}
             </ul>
           </div>
           <div className="box-footer">
             {submitButton} &nbsp;&nbsp;
              <Confirm onConfirm={this.submit.bind(this)}
                    body="Are you sure you want to submit?"
                    confirmText="Confirm"
                    title="Submit Form">
                    <button type="button" id="submitform" className="hidden">Delete Stuff</button>
                </Confirm>
            <a className="btn btn-warning btn-sm" 
             onClick={()=>{
               this.props.edit?this.refs.form.reset(this.props.clientUser):this.refs.form.reset();
               $('select').prop('selectedIndex',0);
               $('#name').val('');
               $('#contactNo').val('');
                $('#secAnswer').val('');
                $('#secquest').val('');
             }}>Reset</a>
            
           </div>

           </Formsy.Form>
         </div>
   </section>
          </div>)
  }
}
