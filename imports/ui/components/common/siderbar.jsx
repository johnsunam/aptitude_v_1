import React,{Component} from 'react';


export default class SiderBar extends Component{
  constructor(props){
    super(props);
  }
  componentDidMount(){
      $('#sideMenu').trigger('click');
    }
  render(){
    
return(<div className="nav-side-menu">
	    <div className="brand">Brand Logo</div>
	    <i className="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" id="sideMenu" data-target="#menu-content"></i>
	  
	        <div className="menu-list">
	  
	            <ul id="menu-content" className="menu-content collapse out">
	                <li>
	                  <a href="#">
	                  <i className="fa fa-dashboard fa-lg"></i> Dashboard
	                  </a>
	                </li>

	                <li  data-toggle="collapse" data-target="#forms" className="collapsed active">
	                  <a href="#"><i className="fa fa-gift fa-lg"></i>Form <span className="arrow"></span></a>
	                </li>
	                <ul className="sub-menu collapse" id="forms">
	                     <li><a href="/aptitude/add-form">Add Form</a></li>
                        <li><a href="/aptitude/manage-form">Manage Form</a></li>
	                </ul>	
                  <li  data-toggle="collapse" data-target="#client" className="collapsed">
	                  <a href="#"><i className="fa fa-gift fa-lg"></i>Client<span className="arrow"></span></a>
	                </li>

	                <ul className="sub-menu collapse" id="client">
	                     <li><a href="/aptitude/add-client">Add Client</a></li>
                  <li><a href="/aptitude/manage-client">Manage Client</a></li>
	                </ul>	 
                  <li  data-toggle="collapse" data-target="#page" className="collapsed ">
	                  <a href="#"><i className="fa fa-gift fa-lg"></i>Page<span className="arrow"></span></a>
	                </li>
                  <ul className="sub-menu collapse" id="page">
	                     <li><a href="/aptitude/add-page">Add Page</a></li>
                  <li><a href="/aptitude/manage-page">Manage Page</a></li>
	                </ul>  
                  <li  data-toggle="collapse" data-target="#workFlow" className="collapsed ">
	                  <a href="#"><i className="fa fa-gift fa-lg"></i>Workflow<span className="arrow"></span></a>
	                </li>
                  <ul className="sub-menu collapse" id="workFlow">
	                    <li><a href="/aptitude/add-workflow">Add Work Flow</a></li>
                  <li><a href="/aptitude/manage-workflow">Manage Work Flow</a></li>
	                </ul>	
                  <li  data-toggle="collapse" data-target="#user" className="collapsed ">
	                  <a href="#"><i className="fa fa-gift fa-lg"></i>User<span className="arrow"></span></a>
	                </li>
                  <ul className="sub-menu collapse" id="user">
	                    <li><a href="/aptitude/add-user">Add User</a></li>
                  <li><a href="/aptitude/manage-user">Manage User</a></li>
	                </ul>              
	            </ul>
	     </div>
            </div>)

  }
  
}
/*{<div className="col-md-2 no_pad">
      <div className="collapse navbar-collapse navbar-ex1-collapse">
        <ul className="nav navbar-nav side-nav">
          <li className=" "><a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user"></i>Form Builder<b className=""><img src="../images/btm_arrow.png"/></b></a>
            <ul className="">
             
              
              <li><a href="javascript:void(0)">Page</a>
                <ul>
                  <li><a href="/aptitude/add-page">Add Page</a></li>
                  <li><a href="/aptitude/manage-page">Manage Page</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li className=" "><a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user"></i>Work Flow Builder<b className=""><img src="../images/btm_arrow.png"/></b></a>
            <ul className="">
              <li><a href="javascript:void(0)">Work Flow</a>
                <ul>
                  <li><a href="/aptitude/add-workflow">Add Work Flow</a></li>
                  <li><a href="/aptitude/manage-workflow">Manage Work Flow</a></li>
                  

                </ul>
              </li>
            </ul>
          </li>
          <li className=" "><a href="javascript:void(0)" className="dropdown-toggle" data-toggle="dropdown"><i className="fa fa-user"></i>User and Roles<b className=""><img src="../images/btm_arrow.png"/></b></a>
            <ul className="">
              <li><a href="javascript:void(0)">User</a>
                <ul>
                  <li><a href="/aptitude/add-user">Add User</a></li>
                  <li><a href="/aptitude/manage-user">Manage User</a></li>
                </ul>
              </li>
              
            </ul>
          </li>
        </ul>
      </div>
  </div>}*/