import React,{Component} from 'react'

export default ClientAdminHeader=(props)=>{
  return(<header className="main-header">   
    <nav className="navbar navbar-fixed-top">
      
      <a href="/client/dashboard" className="logo">
       
        <span className="logo-mini"><b>A</b>LT</span>
       
        <span className="logo-lg"><b>Admin</b>LOGO</span>
      </a>

   
      <div className="navbar-custom-menu">

        <ul className="nav navbar-nav pull-left">
          <li className="user user-menu">
            <a href="manage-user.html" className="dropdown-toggle" data-toggle="dropdown">
              <span className="hidden-xs">MANAGE USER</span>
            </a>
              <ul className="dropdown-menu">
              <li className=""><a href="/client/add-user"><i className="fa fa-circle-o"></i>Add User</a></li>
              <li><a href="/client/manage-user"><i className="fa fa-circle-o"></i>Manage User</a></li>
     
              </ul>
          </li>
        </ul>
        <ul className="nav navbar-nav pull-right">
          <li className="dropdown user user-menu">
            <a href="#" className="dropdown-toggle" data-toggle="dropdown">
              <img src="content/img/user2-160x160.jpg" className="user-image" alt="User Image"/>>
              <span className="hidden-xs">{props.data.contactName?props.data.contactName:props.data.name}</span>
            </a>
            <ul className="dropdown-menu">
            
              <li className="user-header">
                <img src="content/img/user2-160x160.jpg" className="img-circle" alt="User Image"/>

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
           
              <li className="user-body">
                <div className="row">
                  <div className="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div className="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                
              </li>
              <li className="user-footer">
                <div className="pull-left">
                  <a href="#" className="btn btn-default btn-flat">Profile</a>
                </div>
                <div className="pull-right">
                  <a href="#" className="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          

          <li>
            <a href="#" className="dropdown-toggle"  onClick={()=>{
         window.localStorage.setItem('appType',null)
  window.localStorage.setItem('user',null)
  window.localStorage.setItem('loginType',null)
  window.localStorage.setItem('subType',null)
             window.sessionStorage.setItem('user',null);
                    Meteor.logout() }} data-toggle="dropdown">
              <span className="#">LOG OUT</span>
            </a>
          </li>

        </ul>
      </div>

    </nav>
  </header>)
}

