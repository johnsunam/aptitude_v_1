  import React,{Component} from 'react'

export default ClientAdminSidebar=()=>{
  return(<aside className="main-sidebar">
    <section className="sidebar">
      <ul className="sidebar-menu">
       <li><a></a></li>
        <li className="treeview active"> <a href="/client/dashboard">
        <i className="fa fa-dashboard"></i> <span>Dashboard</span>
         <span className="pull-right-container"> <i className="fa fa-angle-left pull-right"></i> </span> </a>
        </li>
        <li className="treeview active"> <a href="#"> <i className="fa fa-pie-chart"></i>
         <span>User</span> <span className="pull-right-container"> 
         <i className="fa fa-angle-left pull-right"></i> </span> </a>
        <ul className="treeview-menu">
          <li className=""><a href="/client/add-user"><i className="fa fa-circle-o"></i>Add User</a></li>
          <li><a href="/client/manage-user"><i className="fa fa-circle-o"></i>Manage User</a></li>
        </ul>
         </li>

       {/*  <li className="treeview-active"><a href="/client/view-statistics">
         <i className="fa fa-bar-chart"></i> Graph</a></li>*/}  
      </ul>
    </section>

  </aside>)
}
