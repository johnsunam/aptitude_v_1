import React,{Component} from 'react'

export default class UserType extends Component {
  constructor(props) {
    super(props)
    this.state={
      user:null
    }
  }
  componentWillMount(){
    let user=window.localStorage.getItem('type');
    if(user!=null){
      switch(user) {
                case 'aptitude-admin':
                FlowRouter.go('/aptitude/add-form');
                  break;
                  case 'client':
                  FlowRouter.go('/client/dashboard')
                  break;
                  case 'App-User':
                  FlowRouter.go('/app/dashboard')
                  break;
              };
    }
    

  }
  render(){
    return(<div>
    <h2>Login As</h2>
<ul>
    {this.props.types.map((type)=>{
      return(<li><input type="radio" name="userType" value={type} onClick={(e)=>{
        window.localStorage.setItem('userType',e.target.value);
        this.setState({user:e.target.value})
        window.localStorage.setItem('type',e.target.value);
        
      }}/>{type}</li>)
    })}
    </ul>
     {this.state.user==null?<span></span>:<button type="button" className="btn btn-default" data-dismiss="modal" onClick={()=>{
        console.log(this.state.user);
        switch (this.state.user) {
          case 'aptitude-admin':
          FlowRouter.go('/aptitude/add-form');
            break;
            case 'client':
            FlowRouter.go('/client/dashboard')
            break;
            case 'App-User':
            FlowRouter.go('/app/dashboard')
            break;
        }
      }}>Login  into    {this.state.user}</button>}
      
    </div>)
  }
}
