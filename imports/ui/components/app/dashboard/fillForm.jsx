import React,{Component} from 'react';
import Alert from 'react-s-alert';
import ShowForm from './showForm';
import IncidentList from '../../../container/incidentList.js'
import TaskTable from '../../../container/taskTable.js';
export default class FillForm extends Component {
  constructor(props) {
    super(props)
	this.state={rules:[],images:[],count:'',
showMessage:'',message:'',detail:'',pages:[],incidentId:'',formdata:'',show:false}
  }


  formEvents(props){
	   let self=this;
     let flowchart=props.data.workflow.flowchart;
     let level=props.data.workflow.level;
     console.log(level)
     self.setState({level:props.data.workflow.level})
    var steps=flowchart['bpmn2:definitions']['bpmn2:process']['bpmn2:userTask']

          console.log(steps)
          var userRoles=self.props.data.user.roles;
          
          var pages=[]
         _.map(steps,function(single,key){
            console.log(key)
             var common=_.intersection(single.detail.roles,userRoles)
             if(common.length>0)
           pages.push({lvl:key,page:single});
          }) 
          self.setState({pages:pages}) 

          var startingPage=_.findWhere(pages,{lvl:0});
        
	
    
  }


  componentDidMount(){
     this.formEvents(this.props);
     

     
}

  componentWillReceiveProps(nextProps) {
 this.formEvents(nextProps)
 this.setState({show:false})
}

 chooseForm(data){
   console.log(data)
   this.setState({show:true,page:data.page,incidentId:data.id,formdata:data.formdata})
   }
hideForm(){
  console.log('hide')
  this.setState({show:false})
}
  render(){
  	return(<div>
    <div id="upper">
     {this.state.pages.map((single)=>{
      return single.lvl===0?<h4><a href='#' 
      onClick={()=>{
        this.setState({show:true})
        this.setState({page:single})
    }}>Register new event</a></h4>:<div><IncidentList chooseForm={this.chooseForm.bind(this)} page={single} workflow={this.props.data.workflow._id}/></div>
    })}
    </div>

   {this.state.show?<div id="forms">
    <ShowForm page={this.state.page} workflow={this.props.data.workflow}
     totallvl={this.props.data.workflow.totallvl} 
     incidentId={this.state.incidentId}
     formdata={this.state.formdata}
     hideForm={this.hideForm.bind(this)}/>
    </div>:<span></span>}
    <h4><a href="#" >Assigned  tasks</a></h4>
    <TaskTable workflow={this.props.id}/>
  	</div>);
   
  }
}