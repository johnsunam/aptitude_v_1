import React,{Component} from 'react';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';


export default class IncidentList extends Component{
    constructor(props){
        super(props);
    }
        
    render(){
        console.log(this.props)
        return(<Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No records found."> 
             {this.props.datas.map((single)=>{
                 console.log(single)
                          return(<Tr className="myTableRow" data={single.formdata}>
                            <Td column="Action" className="actionTd">
                            <a href="#" onClick={(rowData)=>{
                                console.log(rowData)
                                this.props.chooseForm({page:this.props.page,id:single._id,formdata:single.formdata})
                            }}>fill form</a>
                            </Td>
                          </Tr>)
                        })}
        </Table>)
    }
}