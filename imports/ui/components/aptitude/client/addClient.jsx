
import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
import MyInput from '../../common/validator.js'
import ToolTip from 'react-portal-tooltip'
//import ReactConfirmAlert, { confirmAlert } from 'react-confirm-alert'; 
var Confirm = require('react-confirm-bootstrap');

export default class AddClient extends Component {
  constructor(props) {
   super(props)
   this.state={countries:[{name:"Sighapore"},{name:"Hongkong"},{name:'Malasiya'},{name:'America'}],
    saveResult:false,
    edit:this.props.edit,
    client:this.props.client,
  code:"",
  canSubmit: false,
  res:"",
  companyName:"",
  address:'',
  email:'',
  phone:'',
  website:'',
  city:'',
  state:'',
  country:'',
  pincode:'',
  contactName:'',
  contactNo:'',
  roles:[],
  tooltip:'',
  tooltipMessage:'',
  logo:'',
  showMessage:'',
  message:''
    }
  }

  enableButton() {
    this.setState({ canSubmit: true });
  }

  disableButton() {
    this.setState({ canSubmit: false });
  }

  componentDidMount(){
   this.initVar(this.props);
 //   Session.set('showCode',false)
  }
initVar(props){
var client=props.client
    client=client?client:[]
    this.state.edit?this.setState({companyName:client.companyName,
       isTooltipActive: false,
      email:client.email,
      address:client.address,
      phone:client.phone,
      website:client.website,
      city:client.city,
      state:client.state,
      pincode:client.pincode,
      contactName:client.contactName,
      contactNo:client.contactNo,country:client.country,roles:client.roles}):this.setState({companyName:'',
      address:'',
      phone:'',
      website:'',
      city:'',
      state:'',
      pincode:'',
      contactName:'',
      contactNo:'',
      country:'',
      tooltip:"",
      logo:''
      })
}
componentWillReceiveProps(nextProps){
 this.initVar(nextProps);
}

    showTooltip(e) {
      console.log(e.target.name);
      this.setState({tooltip:e.target.name})
        this.setState({isTooltipActive: true})
        this.setState({tooltipMessage:message[`${e.target.name}`]})
    }
    hideTooltip(e) {
      this.setState({tooltip:e.target.id})
        this.setState({isTooltipActive: false})
    }

  shouldComponentUpdate(nextProps, nextState){
    var self=this;
    Tracker.autorun(function(){
      if(Session.equals('confirm',true)){
        if(Session.get('res')==true){
          console.log('helo')
          self.setState({showMessage:true,message:"Client Registered Sucessfully"})
          $('.message').addClass('su')
        }else{
           self.setState({showMessage:true,message:"Email already exits"})
          $('.message').addClass('er')
        }
       Session.set('confirm',false)
      }
    })

    return true;
}

  // saving client to ClientDb
  submit(e){
    var self=this;
    let obj= new crudClass();
      let companyName=$('#companyName').val(),
          address=$('#address').val(),
        	email=$('#email').val(),
        	phone=$('#phone').val(),
        	website=$('#website').val(),
        	city=$('#city').val(),
        	state=$('#state').val(),
        	pincode=$('#pincode').val(),
        	contactName=$('#contactName').val(),
        	contactNo=$('#contactNo').val(),
          country=$('#country').val(),
          logo=this.state.logo
        
    let ran=Random.hexString(7);
    let user=window.localStorage.getItem('user');
    let record=this.props.edit?{id:this.props.client._id,data:{companyName:companyName,address:address,email:email, phone:phone, website:website, city:city, state:state,pincode:pincode,contactName:contactName,contactNo:contactNo,country:country,roles:this.state.roles,logo:logo}}:
      {user:user,data:{code:ran,companyName:companyName,address:address,email:email, phone:phone, website:website, city:city, state:state,pincode:pincode,contactName:contactName,contactNo:contactNo,country:country,roles:this.state.roles,logo:logo}}
    let res=self.state.edit? obj.edit('editClient',record) : obj.create('addClient',record);
    self.setState({saveResult:res,  isShowMessage: true,code:ran})
    self.refs.form.reset();
    self.setState({roles:[]});
    self.setState({logo:''})
    $('select').prop('selectedIndex',0);
    }  
 
 triggerConfirm()
{
  $("#confirm").trigger('click');
}  render(){
    let submitButton=<button className="btn btn-primary btn-sm" type="submit"  disabled={!this.state.canSubmit} ><span>Save</span></button>
         return(<div className="col-md-10 registration_form pad_t50">
      <div className="col-md-8 col-md-offset-2">
        <div className="card"></div>
        <div className="card">
          {this.state.edit?<span className="col-md-offset-9" ><a className="close" style={{paddingRight:10}} data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a></span>:<span></span>}
          <h1 className="title">{this.props.edit?"Edit Client":"Add Client"}</h1>
         <div className="message col-md-offset-1">{this.state.message}</div>
          <div className="form_pad">

          <Formsy.Form ref="form" onValidSubmit={this.triggerConfirm.bind(this)} id="addClient" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
            <div className="row">
              <div className="col-md-6">

                <div className="input-container">
                  <MyInput type="text" help="Enter you company name" maxLength="30" title="Company Name" name="companyName" id="companyName" ref="companyName" value={this.props.edit?this.props.client.companyName:''} required />
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Enter your valid address" maxLength="50" title="Address" name="address" id="address" ref="address" value={this.props.edit?this.props.client.address:''} required />
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="email" help="Please enter valid email address." maxLength="50" title="Email" name="email" validations="isEmail" validationError="This is not a valid email address" ref="email" value={this.props.edit?this.props.client.email:''} required />
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="number" maxLength="10" help="Enter your contact number" title="Phone" name="phone" id="phone" ref="phone" value={this.props.edit?this.props.client.phone:''}/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" maxLength="50" help="Enter your website" title="Website" name="website" id="website" ref="website" value={this.props.edit?this.props.client.website:''}/>
                  <div className="bar"></div>
                </div>
                <label>Add Roles <span style={{color:'red'}}> *</span></label>

              <div className="input-group">
              <input type="text" className="form-control" maxLength="20" ref="roles" placeholder="Roles...."/>
                <span className="input-group-btn">
                 <button className="btn btn-default" type="button" onClick={()=>{
                   let pre=this.state.roles
                   this.refs.roles.value!=''?pre.push(this.refs.roles.value):Alert.warning("role is empty",{
                     position: 'top-right',
                     effect: 'bouncyflip',
                     timeout: 1000
                    })
                     this.setState({roles:pre})
                     this.refs.roles.value=''
                  }}>Add</button>
                </span>
              </div>
              <div>
            <ul>
                {this.state.roles.map((role)=>{
                return(<li>{role}<a href="#" id={role} onClick={(e)=>{
                  let pre=_.without(this.state.roles,e.target.id)
                  this.setState({roles:pre})
                }}><i id={role} className="fa fa-times"></i></a></li>)
                })}
            </ul>
              </div>
              <div className="input-group">
              <label className="control-label">Select Logo</label>
              <input id="input-1" type="file" class="file" onChange={(e)=>{
                var preview = document.querySelector('#logo');
                let self=this;
                var file    = document.querySelector('input[type=file]').files[0];
                var reader  = new FileReader();
                reader.addEventListener("load", function () {
                preview.src = reader.result;
                self.setState({logo:reader.result})
              }, false);

              if (file) {
                reader.readAsDataURL(file);
              }
              }}/>
              <img src="" id="logo" height="200" alt="Image preview..."/>
              </div>
              

        
              </div>

              <div className="col-md-6">

                <div className="input-container">
                  <MyInput type="text" help="Enter your city name" maxLength="25" title="City" name="city" ref="city" value={this.props.edit?this.props.client.city:''}/>
                  <div className="bar"></div>
                </div>
                <div className="input-container">
                  <MyInput type="text" help="Enter your state" maxLength="25" title="State" name="state" ref="state" value={this.props.edit?this.props.client.state:''}/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  {/*<select id="countries" ref="country">
                    <option>{this.props.edit?this.state.country:<span>Country<strong style={{color:'red'}}>*</strong></span>}</option>
                      {this.state.countries.map((country)=>{
                      return(<option>{country.name}</option>)
                     })}
                  </select>*/}
                 <MyInput type="text" help="Enter your country" maxLength="25" title="Country" name="country" ref="country" id="country" value={this.props.edit?this.props.client.country:''}/>
                  <div className="bar"></div>
                  
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Enter the pincode" maxLength="5" title="Pincode" name="pincode" ref="pincode" id="pincode" value={this.props.edit?this.props.client.pincode:''}/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="text" help="Pick up your contact name" maxLength="15" title="Contact Name" name="contactName" id="contactName" ref="contactName" value={this.props.edit?this.props.client.contactName:''}/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                  <MyInput type="number" help="Enter your contact number" maxLength="10" name="contactNo" title="Contact No" id="contactNo" ref="contactNo" value={this.props.edit?this.props.client.contactNo:''}/>
                  <div className="bar"></div>

                </div>
                
              </div>
               

              </div>
            <div className="">
            <Confirm onConfirm={this.submit.bind(this)}
                    body="Are you sure you want to submit?"
                    confirmText="Confirm"
                    title="Submit Form">
                    <button id="confirm" className="hidden"></button>
                </Confirm>
             {submitButton}&npsp;&npsp
            <a className="btn btn-warning btn-sm" 
             onClick={()=>{
               
               this.props.edit?this.refs.form.reset(this.props.client):this.refs.form.reset();
               //this.props.edit? this.setState({roles:this.props.client.roles}):this.setState({roles:[]});
               // this.props.edit?this.setState({logo:this.props.client.logo}):this.setState({logo:''});
             }}>Reset</a>
             
            </div>
            </Formsy.Form>
          </div>

        </div>
		  </div>
      <ToolTip active={this.state.isTooltipActive} position="top" arrow="center" parent={`#${this.state.tooltip}`}>
                    <div>
                        <p>{this.state.tooltipMessage}</p>

                    </div>
                </ToolTip>

    </div>)
  }
}
