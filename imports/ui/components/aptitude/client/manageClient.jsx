import React ,{Component} from 'react'
import Paginate from '../../common/paginator.jsx'
import AddClient from '../../../container/editAddedClient.js'
import crudClass from '../../common/crudClass.js'
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ManageClient extends Component {

  constructor(props) {
   super(props)
   this.state = {
    datas:{},
    rows:props.clients
   }
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps)
    this.setState({rows:nextProps.clients})
  }

  render(){
    let self = this;
    return(
      <div className="col-md-10 " >
      <div className="col-md-offset-1">
        <h1 className="title">Manage Client</h1>

        <Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true}>
            {this.state.rows.map(function(row) {
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Company Name">{row.companyName}</Td>
                            <Td column="Email">{row.email}</Td>
                            <Td column="Phone">{row.phone}</Td>
                            <Td column="Website">{row.website}</Td>
                            <Td column="Code">{row.code}</Td>
                            <Td column="Action">
                              <div>
                              <ReactTooltip id='edit' data-tip  type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                                <a href="#" className="round-primary edit" data-tip data-for='edit' onClick={()=>{
                                 self.setState({datas:row})
                                }}  data-toggle="modal" data-target="#myModal"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
                                <ReactTooltip id='delete' type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                                <a href="#"  className="round-danger" data-tip data-for='delete'  onClick={()=>{
                                  let obj=new crudClass()
                                 obj.delete('deleteClient',row._id)
                                }} data-toggle="confirmation"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
                            </div>
                            </Td>
                        </Tr>
                    )
                })}
        </Table>
        </div>
           <div className="modal fade" id="myModal">
              {this.state.datas._id?<AddClient edit="true" client={this.state.datas}/>:console.log(this.state.datas)}
     
          </div>
        </div> 
      )
  }


//   buttonFormatter(cell, row){
//     let self=this;
//     return (
//       <div>
//           <a href="#" className="btn btn-primary btn-sm" disabled={row.status?'disabled':''}  onClick={()=>{
//             console.log(row)
//            self.setState({datas:row})
//           }}  data-toggle="modal" data-target="#myModal"><i className="fa fa-pencil-square-o" aria-hidden="true"></i></a>
//           <a href="#" disabled={row.status?'disabled':''}  className="btn btn-danger btn-sm" onClick={()=>{
//             let obj=new crudClass()
//            obj.delete('deleteClient',row._id)
//           }} data-toggle="confirmation"><i className="fa fa-trash-o" aria-hidden="true"></i></a>
//       </div>)
//   }

//   render(){
    
//     const options = {
//       sizePerPage: 5,  // which size per page you want to locate as default
//       pageStartIndex: 0, // where to start counting the pages
//       paginationSize: 4,  // the pagination bar size.
//       prePage: 'Prev', // Previous page button text
//       nextPage: 'Next', // Next page button text
//       firstPage: 'First', // First page button text
//       lastPage: 'Last', // Last page button text
//     };

   
   
//       return(
//         <div className="col-md-10 whiteColor" >
//       <div className="col-md-10 col-md-offset-1">
//         <h1 className="title">Manage Client</h1>
//       <BootstrapTable data={this.state.rows} options={ options } headerStyle={ { background: 'lightblue !important'} } trClassName='tr-string-example' pagination>
//         <TableHeaderColumn dataField='companyName' dataSort={ true }>Name</TableHeaderColumn>
//         <TableHeaderColumn dataField='email' isKey dataSort={ true }>Email</TableHeaderColumn>
//         <TableHeaderColumn dataField='phone' dataSort={ true }>Phone</TableHeaderColumn>
//         <TableHeaderColumn dataField='website' dataSort={ true }>Website</TableHeaderColumn>
//         <TableHeaderColumn dataField='code' dataSort={ true }>Code</TableHeaderColumn>
//         <TableHeaderColumn dataField='button' dataFormat={this.buttonFormatter.bind(this)}>Action</TableHeaderColumn>
//     </BootstrapTable> 
     
//       </div>
//           <div className="modal fade" id="myModal">
//             <div className="modal-dialog" role="document">
//               <div className="modal-content">
//                 <div className="modal-header">
//                   <button type="button" className="close" data-dismiss="modal" aria-label="Close">
//                     <span aria-hidden="true">&times;</span>
//                     <span className="sr-only">Close</span>
//                   </button>
//                   <h4 className="modal-title">Edit Client</h4>
//                 </div>
//                 <div className="modal-body">
//                   {this.state.datas._id?<AddClient edit="true" client={this.state.datas}/>:console.log(this.state.datas)}
//                 </div>
//                 <div className="modal-footer">
//                 </div>
//               </div>
//             </div>
//           </div>
//     </div>

//   );
// }
}