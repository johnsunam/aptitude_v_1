//edit,delete and lists client
import React ,{Component} from 'react'
import AddWorkFlow from '../../../container/addWorkFlow.js'
import crudClass from '../../common/crudClass.js'
import orderBy from 'lodash/orderBy';
import Paginate from '../../common/paginator.jsx'
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'

export default class ManageWorkFlow extends Component {
  constructor(props) {
   super(props)
   this.state = {
     rows:props.data.workflows,
    
   }
  }

componentDidMount(){
  console.log(this.props.data.workflows)
  this.setState({rows:this.props.data.workflows})
}
componentWillReceiveProps(){
  this.setState({rows:this.props.data.workflows})
}
  render(){
    var self=this;
      return(<div><Table className="table table-bordered table-hover table-striped margin-top20" itemsPerPage={30} sortable={true} noDataText="No Workflow found.">
            {this.props.data.workflows.map(function(row) {
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Name">{row.name}</Td>
                            <Td column="Description">{row.description}</Td>
                            <Td column="Status">{row.status}</Td>
                            <Td column="Action">
                              <div>
                              <ReactTooltip id='edit' type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                                <a href="#" className="round-primary edit" data-tip data-for='edit' onClick={()=>{
                                   self.setState({datas:row})
                                  
                                 }}  data-toggle="modal" data-target="#myModal" ><i 
                                className="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;

                                <ReactTooltip id='delete' type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                                 <a href="#" className="round-danger" data-tip data-for='delete'   onClick={()=>{
                                  let obj=new crudClass()
                                  obj.delete('deleteWorkFlow',row._id)
                                }}><i className="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                                <a href={`https://aptitude-workflow.herokuapp.com/${row._id}`} target='_blank' className="round-primary"><i className="fa fa-file-code-o" aria-hidden="true"></i></a>&nbsp;&nbsp;
                            </div>

                                   {/* https://aptitude-workflow.herokuapp.com*/}
                            </Td>
                             
                        </Tr>
                    )
                })}

        </Table>
        {this.state.datas?<div className="modal fade" id='myModal' tabindex="-1" user="dialog" aria-labelledby="myModalLabel">
<AddWorkFlow edit="true" workflow={this.state.datas}/>
</div>:<span></span>}
        </div>
  );
}
}