//adding new pages to the pagedb
import React ,{Component} from 'react'
import {Random } from 'meteor/random'
import crudClass from '../../common/crudClass.js'
var message = require('../../common/message.json');
import Alert from 'react-s-alert';
import {Session} from 'meteor/session';
import ReactConfirmAlert, { confirmAlert } from 'react-confirm-alert'; 
//import 'react-confirm-alert/src/react-confirm-alert.css'
var Confirm = require('react-confirm-bootstrap');
export default class AddPage extends Component {
  constructor(props) {
   super(props)
   this.state={
     saveResult:false,
     edit:props.edit,
    canSubmit: false,
	confirm:Session.get('confirm'),
	res:"",
  name:'',
  clName:'',
  formName:'',
  previewURL:'',
  publishURL:'',
  metakeys:'',
  showMessage:'',
  message:''
}

  }
  componentDidMount(){
    let page=this.props.page;
    console.log(page);
    this.state.edit?this.setState({name:page.name,
      clientName:page.clientName,
      formName:page.formName,
      previewURL:page.previewURL,
      publishURL:page.publishURL,
      metakeys:page.metakeys,
      status:page.status}):this.setState({name:"",
      clientName:"",
      formName:"",
      previewURL:"",
      publishURL:"",
      metakeys:""})
  }
  shouldComponentUpdate(nextProps, nextState){
    var self=this;
    Tracker.autorun(function(){
     
        if(Session.equals('confirm',true)){
        if(Session.get('res')==true){
          console.log('helo')
          self.setState({showMessage:true,message:"Page Saved Sucessfully"})
          $('.message').addClass('su')
        }else{
           self.setState({showMessage:true,message:"Error Saving Page"})
           $('.message').addClass('er')
          
        }
              Session.set('confirm',false)
      }
      
    })

    return true;
}


  enableButton() {
    this.setState({ canSubmit: true });
  }
  disableButton() {
    this.setState({ canSubmit: false });
  }

  //add page to PageDb
  submit(e){
   var self=this;
    let name=$('#name').val(),
    clientName=this.refs.clientName.value,
    	formName=$( "#formName option:selected" ).text(),
    	formId=this.refs.formName.value,
     
       admin=window.localStorage.getItem('user'),
       user=admin?admin:Meteor.userId();
       console.log(this.props.data.forms)
       form=_.findWhere(this.props.data.forms,{_id:formId});
    let record=this.props.edit?{id:this.props.page._id,data:{name:name,clientName:clientName,form:form,formName:formName, status:status}}:
    {user:user,name:name,clientName:clientName,form:form,formName:formName}
    let obj= new crudClass();
    let res;
    if(self.state.edit){
          res=obj.edit('editPage',record)
    }
    else{
      var exits=_.findWhere(this.props.data.pages,{name:record.name})
      if(exits){
          alert('page name exits')
      }else{
        res=obj.create('addPage',record)
      }
    }
   
   
   
   
    this.setState({name:"",
    clientName:"",
    formName:"",
    previewURL:"",
    publishURL:"",
    metakeys:""})
	this.setState({saveResult:res})
  this.refs.form.reset();
  $('select').prop('selectedIndex',0);
  }
  triggerConfirm()
{
  $("#confirm").trigger('click');
}
  render(){
    console.log(this.state.showMessage)
     let submitButton=<button type="submit" className="btn btn-primary btn-sm" disabled={!this.state.canSubmit}><span>Save</span></button>
     let page=this.props.page;
      return(<div className="col-md-10 registration_form pad_t50">
      <div className="col-md-6 col-md-offset-3">
        <div className="card"></div>
        <div className="card">
        {this.state.edit?<span className="col-md-offset-9" ><a className="close" style={{paddingRight:10}} data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </a></span>:<span></span>}
          <h1 className="title">{this.state.edit?"Edit Page":"Create page for the client"}</h1>
         <div className="message col-md-offset-1">{this.state.message}</div>
          <div className="form_pad">

           <Formsy.Form ref="form" onValidSubmit={this.triggerConfirm.bind(this)} id="addPage" onValid={this.enableButton.bind(this)} onInvalid={this.disableButton.bind(this)}>
            <div className="row">
              <div className="col-md-12">
                <div className="input-container">
                  <MyInput type="text" help="Enter your page name" maxLength="30" title="Page Name" name="name" id="name" value={this.props.edit?page.name:''}  ref="name" required/>
                  <div className="bar"></div>
                </div>

                <div className="input-container">
                 <select ref="clientName" placeholder="client">
                 <option>{this.props.edit?page.clientName:'choose client'}</option>
                 {this.props.data.clients.map((client)=>{

                return(<option>{client.companyName}</option>)
                 })}
                 </select>
                </div>

                <div className="input-container" placeholder="form">
                 <select ref="formName" id="formName">
                 <option>{this.props.edit?page.formName:'choose form'}</option>
                  {this.props.data.forms.map((form)=>{
                return(<option value={form._id}>{form.name}</option>)
                 })}
                 </select>
                </div>
                 

              </div>
            </div>

            <div className="">
              {submitButton}&nbsp;&nbsp;&nbsp;
             <a className="btn btn-warning btn-sm" 
             onClick={()=>{
               
               this.props.edit?this.refs.form.reset(this.props.page):this.refs.form.reset();
              // $('select').prop('selectedIndex',0);
             }}>Reset</a>
			       
            </div>
             
            </Formsy.Form>
            <Confirm onConfirm={this.submit.bind(this)}
                    body="Are you sure you want to submit?"
                    confirmText="Confirm"
                    title="Submit Form">
                    <button id="confirm" className="hidden"></button>
                </Confirm>
          </div>
        </div>
      </div>

    </div>)
  }
}
