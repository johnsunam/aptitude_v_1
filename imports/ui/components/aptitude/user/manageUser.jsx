//edit,delete and lists client
import React ,{Component} from 'react'
import Paginate from '../../common/paginator.jsx'
import AddUser from './addUser.jsx'
import crudClass from '../../common/crudClass.js'
import orderBy from 'lodash/orderBy';
import ReactTable,{Table,Tr,Td,Th} from 'reactable';
import ReactTooltip from 'react-tooltip'
export default class ManageUser extends Component {
  constructor(props) {
   super(props)
   this.state = {datas:''
   }
  }

  render(){
    let self=this;
      return(
        <div className="col-md-10 " >
      <div className="col-md-10 col-md-offset-1">
        <h1 className="title">Manage User</h1>
        <Table className="table table-bordered table-hover table-striped margin-top20" noDataText="No User found." itemsPerPage={30} sortable={true}>
            {this.props.users.map(function(row) {
                    return (
                        <Tr key={row.key} className="myTableRow">
                            <Td column="Name">{row.name}</Td>
                            <Td column="Phone">{row.mobile}</Td>
                            <Td column="Email">{row.email}</Td>
                            <Td column="Action" className="actionTd">
                              <div>
                               <ReactTooltip id='edit'  type='info'>
                                <span>Edit</span>
                              </ReactTooltip>
                                <a href="#" data-tip data-for="edit" className="round-primary edit" onClick={()=>{
                                  self.setState({datas:row})
                                }}  data-toggle="modal" data-target="#myModal" ><i 
                                className="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;
                      <ReactTooltip id='delete'  type='info'>
                                <span>Delete</span>
                              </ReactTooltip>
                       <a href="#" className="round-danger" data-tip data-for="delete" onClick={()=>{
                                  let obj=new crudClass()
                                  obj.delete('deleteUser',row.id)
                                }} disabled={row.status?'disabled':''} ><i className="fa fa-trash-o"></i></a>
                            </div>
                            </Td>
                        </Tr>
                    )
                })}
        </Table>
      
      </div>
      {this.state.datas._id?<div className="modal fade" id='myModal' tabindex="-1" user="dialog" aria-labelledby="myModalLabel">
<AddUser edit="true" user={this.state.datas}/>
</div>:<span></span>}
      
    </div>

  );
}
}