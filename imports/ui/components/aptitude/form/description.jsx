import React,{Component} from 'react'

export default class Description extends Component{
    constructor(props){
        super(props)
        this.state={
            description:''
        }
    }
    render(){
        return(<div className="slider text-center">
                   <h2>Description</h2>
                   <textarea  id="formdescription" cols="8" rows="3" className="form-control" onKeyDown={(e)=>{
                     this.setState({
                       description:e.target.value
                     });
                   }} placeholder="describe your form" required="required">{this.state.description}</textarea>
                   <br/>
                   <button className="btn btn-xs btn-primary back-btn pull-left navigate-btn" onClick={()=>{
                       this.props.changeCompnent('SearchField');
                       this.props.formDescription(this.state.description);
                   }}>Previous</button>
                   <button className="btn btn-xs btn-primary next-btn pull-right navigate-btn" onClick={()=>{
                       this.props.changeCompnent('Save');
                       this.props.formDescription(this.state.description);
                   }}>Next</button>
               </div>)
    }
}