import { composeWithTracker } from 'react-komposer';
import {PageDb} from '../../api/page/collection/page.collection.js'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import ManagePage from '../../ui/components/aptitude/page/managePage.jsx'
const composer = ( props, onData ) => {
    var subcription=Meteor.subscribe('getPage');
    var clientSubcription=Meteor.subscribe('getClient');
    var admin =Meteor.subscribe("getAptitudeAdmin")
    if(admin.ready()){

      let user=window.localStorage.getItem('user');
      let f=AptitudeAdminDb.findOne({user:user});
        var pages=PageDb.find({user:user}).fetch();
        var clients=ClientDb.find({user:{$in:f.companies}}).fetch();
        let data={pages:pages,clients:clients}
        onData( null, {data } )
      }

  };


export default composeWithTracker(composer)(ManagePage);
