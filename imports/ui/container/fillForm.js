import { composeWithTracker } from 'react-komposer';
import FillForm from '../components/app/dashboard/fillForm.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js';
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
const composer = ( props, onData ) => {
    var clientUser=Meteor.subscribe('getClientUser')
    var subcription=Meteor.subscribe('getWorkFlow');
    subcription=Meteor.subscribe('getClient')
        if(subcription.ready()){
            console.log(window.localStorage.getItem('appType'))
            let workflow=WorkflowDb.findOne({_id:props.id});
            if(window.localStorage.getItem('appType')=="client-admin"){
                let c=ClientDb.findOne({user:Meteor.userId()})
                let data={workflow:workflow,user:c}
                 onData( null, {data} )
            }else{
                 let c=ClientUserDb.findOne({user:Meteor.userId()})
                let data={workflow:workflow,user:c}
                 onData( null, {data} )
            }
        
       
      }

  };


export default composeWithTracker(composer)(FillForm);
