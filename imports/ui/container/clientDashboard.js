import { composeWithTracker } from 'react-komposer';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection'
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection.js'
import ClientAdminPages from '../components/client/dashboard/dashboard.jsx'
const composer = ( props, onData ) => {
  let email=Meteor.user()

    var subcription=Meteor.subscribe('getPage');
     let cllient=Meteor.subscribe('getClient');
     let workflow=Meteor.subscribe('getWorkFlow')
    if(subcription.ready()){
      let workflows=WorkflowDb.find({client:window.localStorage.getItem('company')}).fetch();
       onData( null, {workflows} )
   }

  };


export default composeWithTracker(composer)(ClientAdminPages);
