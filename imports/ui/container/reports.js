import { composeWithTracker } from 'react-komposer';
import Reports from '../components/client/reports/reports.jsx';
import {WorkflowDb} from '../../api/workflow/collection/workflow.collection.js'
import {WorkflowDataDb} from '../../api/formdata/collection/workflowData.collection.js'
import {ClientUserDb} from '../../api/clientUser/collection/clientUser.collection';
import {ClientDb} from '../../api/clients/collection/client.collection.js';

const composer = ( props, onData ) => {
  var clientUser=Meteor.subscribe('getClientUser')
  let subscription=Meteor.subscribe("getWorkFlow")
  let client=Meteor.subscribe("getClient")
  let workflowDatas=Meteor.subscribe("getWorkflowDatas");
   if(workflowDatas.ready()){
     console.log(window.localStorage.getItem('appType'))
     let c;
      if(window.localStorage.getItem('appType')=="client-admin")
      {
        c=ClientDb.findOne({user:Meteor.userId()})
      }else if(window.localStorage.getItem('appType')=="aptitude"){
        c=ClientDb.findOne({user:window.localStorage.getItem('user')})
      }
      else{
          c=ClientUserDb.findOne({user:Meteor.userId()});
      }
      
        let workflow=WorkflowDb.findOne({_id:props.workflow});
        let clientUsers=workflow.action?ClientUserDb.find({$and:[{roles:{$in:workflow.action.roles}},{companies:{$in:[window.localStorage.getItem('company')]}}]}).fetch():[];
        
        let workflowdatas=WorkflowDataDb.find({workflow:props.workflow}).fetch();
         let data={workflow:workflow,workflowdatas:workflowdatas,user:c,clientUsers:clientUsers};
         
         onData( null, {data} )
  };    

}
export default composeWithTracker(composer)(Reports);
