import { composeWithTracker } from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import Gate from '../components/common/gate.jsx'
const composer = ( props, onData ) => {

    if(Meteor.userId()){
      console.log(props.role);
      switch (props.role) {
        case 'aptitude-admin':
          FlowRouter.go('/aptitude/add-form')
        break;
        case 'client':
          FlowRouter.go('/client/dashboard')
      break;
        case 'App User':
          FlowRouter.go('/app/dashboard')
      break;
          default:
          FlowRouter.go('/aptitude/login')
      }

    }
    else {
      FlowRouter.go('/aptitude/login')
    }

  };


export default composeWithTracker(composer)(Gate);
