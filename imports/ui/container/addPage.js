
import { composeWithTracker } from 'react-komposer';
import {ClientDb} from '../../api/clients/collection/client.collection.js'
import {FormDb} from '../../api/form/collection/form.collection.js'
import {AptitudeAdminDb} from '../../api/aptitudeAdmin/collection/aptitude.admin.collection.js'
import {PageDb} from '../../api/page/collection/page.collection.js';
import AddPage from '../components/aptitude/page/addPage.jsx'
const composer = ( props, onData ) => {
    var pageSub=Meteor.subscribe('getPage');
    var clinetSubcription=Meteor.subscribe('getClient');
    var formSubcription=Meteor.subscribe('getForm');
    if(formSubcription.ready()){
      var user=window.localStorage.getItem('user')
      var a=AptitudeAdminDb.findOne({user:user});
        var clients=ClientDb.find({user:{$in:a.companies}}).fetch();
        var forms=FormDb.find({user:user}).fetch();
        var pages=PageDb.find().fetch();
        let data={clients:clients,forms:forms,pages:pages}
        onData( null, {data} )
      }

  };


export default composeWithTracker(composer)(AddPage);
