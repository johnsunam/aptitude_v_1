import { composeWithTracker } from 'react-komposer';
import {FlowRouter} from 'meteor/kadira:flow-router';
import ClientAdminLayout from '../layouts/clientAdminLayout.jsx';

const composer = ( props, onData ) => {
      if(Meteor.userId()){
       let user=Meteor.userId()
      onData( null, {user} )

      }
    else{
      FlowRouter.go('/aptitude/login')
    }





  };


export default composeWithTracker(composer)(ClientAdminLayout);
