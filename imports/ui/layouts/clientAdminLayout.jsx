import React,{Component} from 'react'
import Header from '../container/clientAdminHeader.js';
import Footer from '../components/common/footer.jsx'
import SiderBar from '../components/common/clientAdminSidebar.jsx';
import commonImports from '../components/common/commonImports.jsx'
import Alert from 'react-s-alert';

export default class ClientAdminLayout extends Component {
  constructor(props) {
    super(props)
  }
  choose_reports(id){
   
  }
  render(){


    return(<div className="">
      <div className="wrapper">
      <Header/>
      <div class="clearfix"></div>
      <div className="content-wrapper ">
    {this.props.content}
      </div>
    </div>
    <Footer/>
    <Alert stack={{limit: 3}}/>
  </div>)
  }
}
