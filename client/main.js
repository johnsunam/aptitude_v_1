    import '../imports/startup/client/routes.js'
    import { Sparkpost } from 'meteor/agoldman:sparkpost-mail';

Meteor.startup(()=>{
    Sparkpost.config('7d83a70ce6aebb04cac288d363d3e3ca8087b6d2');
if(!getCookie('loginStatus')) {
  window.localStorage.setItem('appType',null)
  window.localStorage.setItem('user',null)
  window.localStorage.setItem('loginType',null)
  window.localStorage.setItem('subType',null)
  
  Meteor.logout();
}
})

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}
